from flask import Flask,render_template,request,Response
from price_comp import comp
from prod_comp import detail
import os
import pandas as pd
app=Flask(__name__)

@app.route("/")
@app.route("/home")
def home_page():
    try:
        return render_template("home.html")
    except:
        return render_template("error.html")

@app.route("/result",methods=['post', 'get'])
def result_page():
    try:
        if request.method=='POST':
            x=request.form.get('nm1')
            print(x)
            x_name=x

            cont=True
            while cont:
                try:
                    items=comp(x)
                    cont=False
                    
                except:
                    cont=True
        
        return render_template("result.html",items=items,x_name=x_name)
    except:
        return render_template("error.html")

@app.route("/price")
def price():
    try:
        return render_template("price_input.html")
    except:
        return render_template("error.html")

@app.route("/prod")
def product():
    try:
        return render_template("prod_input.html")
    except:
        return render_template("error.html")


@app.route("/result2", methods=['post', 'get'])
def product_result():
    try:
       if request.method=='POST':
            one=request.form.get('nm1')
            two=request.form.get('nm2')
            print(one)
            print(two)
            det=detail(one,two)
            print(det)
            comb=one +' and '+ two
            return render_template("result_2.html",det=det,comb=comb)
    except:
        return render_template("error.html")

@app.route("/result3")
def product_result_qrComp():
    try:
        try:
            lines_list = open('dummy.txt').read().splitlines()
        except:
            return render_template('error_qrComp.html')
        print(lines_list)
        os.remove('dummy.txt') 
        one=lines_list[0]
        two=lines_list[1]
        det=detail(one,two)
        print(det)
        comb='QR Comparison are: '
        return render_template("result_2.html",det=det,comb=comb)
    except:
        return render_template("error.html")

@app.route('/redeem')
def redeem():
    return render_template("redeem.html")
    
@app.route('/qr')
def qr_disp():
    return "<h1>ToDo</h1>"

@app.route('/qrComp')
def qrComp_disp():
    return "<h1>ToDo</h1>"

@app.route('/error')
def error():
    return render_template('error.html')

if __name__ == '__main__':
    app.run(debug=True)
